# OpenCV Lowerbody Detection

## Resources
- [Detecting people with YOLO and OpenCV.](https://medium.com/@luanaebio/detecting-people-with-yolo-and-opencv-5c1f9bc6a810)
- [MediaPipe Face Detection](https://google.github.io/mediapipe/solutions/face_detection.html)
- [Human Detection using Haar Cascade classifier and OpenCv in Python](https://www.youtube.com/watch?v=u0c6Wd04gUE)
- [Fullbody Detection Using OpenCV Haar Cascades](https://stackoverflow.com/questions/35923072/fullbody-detection-using-opencv-haar-cascades)
- [Body detection using haarcascade](https://answers.opencv.org/question/42049/body-detection-using-haarcascade/)
- [OpenCv haarcascades](https://github.com/opencv/opencv/tree/4.x/data/haarcascades)
- [simple way to get Z "depth" in OpenCV](https://stackoverflow.com/questions/28929503/simple-way-to-get-z-depth-in-opencv)
- [Basic motion detection and tracking with Python and OpenCV](https://pyimagesearch.com/2015/05/25/basic-motion-detection-and-tracking-with-python-and-opencv/)
- [Face and Movement Tracking Pan-Tilt System with Raspberry Pi and OpenCV](https://core-electronics.com.au/guides/Face-Tracking-Raspberry-Pi/)
- [Waveshare GitHub](https://github.com/waveshare/JETANK)
- [Face Recognition with Local Binary Patterns (LBPs) and OpenCV](https://pyimagesearch.com/2021/05/03/face-recognition-with-local-binary-patterns-lbps-and-opencv/)
- [Opencv LBPHFaceRecognizer Train data in Python](https://answers.opencv.org/question/76996/opencv-lbphfacerecognizer-train-data-in-python/)
- [Local Binary Patterns with Python & OpenCV](https://pyimagesearch.com/2015/12/07/local-binary-patterns-with-python-opencv/)
- [Face Recognition: Understanding LBPH Algorithm](https://towardsdatascience.com/face-recognition-how-lbph-works-90ec258c3d6b)
- [Face Recognition Based On LBPH Algorithm.](https://blog.devgenius.io/face-recognition-based-on-lbph-algorithm-17acd65ca5f7)


